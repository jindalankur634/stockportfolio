import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Portfolio from "./views/Portfolio";
import Home from "./views/Home";
import Navbar from "./views/Navbar";
import Login from "./views/login";
import StockFullDetail from "./views/StockFullDetail";
import Graph from "./views/graphView";
import PrivateAuthRoute from "./views/PrivateAuthRoute";
function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/login" element={<Login />} />

          <Route
            path="/"
            element={
              <PrivateAuthRoute>
                <Home />
              </PrivateAuthRoute>
            }
          />
          <Route
            path="/portfolio"
            element={
              <PrivateAuthRoute>
                <Portfolio />
              </PrivateAuthRoute>
            }
          />
          <Route
            path="/stock/:name"
            element={
              <PrivateAuthRoute>
                <StockFullDetail />
              </PrivateAuthRoute>
            }
          />
          <Route
            path="/graph/:name"
            element={
              <PrivateAuthRoute>
                <Graph />
              </PrivateAuthRoute>
            }
          />
        </Routes>
      </Router>
    </>
  );
}

export default App;
