import React, { useState, useEffect } from "react";
import { useLocation } from "react-router";
import {
  Button,
  Card,
  CardBody,
  CardSubtitle,
  CardTitle,
  Col,
  Row,
} from "reactstrap";
import Navbar from "./Navbar";
import stocks from "../data/stock.json";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import GraphView from "./graphView";
const Portfolio = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const quantity = location.pathname.split("/")[2];
  const buy_price = location.pathname.split("/")[3];
  const id = location.pathname.split("/")[1];
  const [stock, setStock] = useState([]);
  const token = localStorage.getItem("token");
  const allStockData = localStorage.getItem("allstock");
  const [graphView, setGraphView] = useState(false);
  const [compName, setCompName] = useState();

  useEffect(() => {
    settingStockData();
  }, []);

  function settingStockData() {
    if (allStockData) {
      const data = JSON.parse(allStockData);
      setStock(data);
    }
  }

  function calculate() {
    let amt = 0;
    for (let i = 0; i < stock.length; i++) {
      let amount = stock[i].BUY_PRICE * stock[i].QUANTITY;
      amt = amt + amount;
    }
    return amt;
  }

  function sellStock(e) {
    const { target } = e;
    const { name } = target;

    const buyStockList = localStorage.getItem("allstock");
    let remainingStocks = [];
    if (buyStockList != null) {
      remainingStocks = JSON.parse(buyStockList);
      let cName = remainingStocks.filter((e) => e.SYMBOL == name);
      let filterBuyStock = remainingStocks.filter((e) => e.SYMBOL != name);
      localStorage.setItem("allstock", JSON.stringify(filterBuyStock));
      setStock(filterBuyStock);
      toast.success(`${cName[0].COMPANY} Stock Holding Successfully Sold.`);
    }
  }

  function settingCompName(e){
    if(e?.COMPANY){
      setCompName(e.COMPANY);
    }
  }

  return (
    <>
      <div className="bg-dark fullviewOuterDiv overflow-auto">
        <Navbar />
        <div className="display-5 text-white m-2">Portfolio</div>
        <div>
          {stock.length > 0 || stock != "" ? (
            stock.map((e, idx) => {
              return (
                <Card className="w-100 text-white bg-dark">
                  <CardBody>
                    <Row>
                      <Col>
                        <Row>
                          <Col className="col-5">
                            <h2 className="">Company Name : </h2>
                          </Col>
                          <Col className="d-flex justify-content-left">
                            <h2> {e.COMPANY}</h2>
                          </Col>
                        </Row>
                        <Row>
                          <Col className="col-5">
                            <h4 className="">Company Symbol : </h4>
                          </Col>
                          <Col className="d-flex justify-content-left">
                            <h4> {e.SYMBOL}</h4>
                          </Col>
                        </Row>
                      </Col>
                      <Col>
                        <Row>
                          <Col className="col-5">
                            <h5 className="d-flex justify-content-end">
                              Buy Price :{" "}
                            </h5>
                          </Col>
                          <Col className="d-flex justify-content-left">
                            <h5>{e.BUY_PRICE}</h5>
                          </Col>
                        </Row>
                        <Row>
                          <Col className="col-5">
                            <h5 className="d-flex justify-content-end">
                              Quantity Purchased :
                            </h5>
                          </Col>
                          <Col className="d-flex justify-content-left">
                            <h5>{e.QUANTITY}</h5>
                          </Col>
                        </Row>
                        <Row>
                          <Col className="col-5">
                            <h5 className="d-flex justify-content-end">
                              Stock Purchase Amt :
                            </h5>
                          </Col>
                          <Col className="d-flex justify-content-left">
                            <h5>{(e.QUANTITY * e.BUY_PRICE).toFixed(2)}</h5>
                          </Col>
                        </Row>
                        <Row>
                          <Col className="col-8 d-flex justify-content-center">
                            <Button
                              className="btn-success col-5 m-1"
                              onClick={() => {
                                settingCompName(e)
                                setGraphView(!graphView);
                              }}
                              name = {e.COMPANY}
                            >
                              View Chart
                            </Button>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              );
            })
          ) : (
            <div className="display-4 text-danger col-12 d-flex justify-content-center">
              No Stock is Purchased
            </div>
          )}
          {stock.length > 0 || stock != "" ? (
            <h1 className="m-2 text-white col-10 d-flex justify-content-end">
              Total Amount : {calculate().toFixed(2)}
            </h1>
          ) : (
            ""
          )}
          {graphView == true ? (
            <GraphView setGraphView={setGraphView} graphView={graphView} compName={compName} />
          ) : (
            ""
          )}
        </div>
      </div>
    </>
  );
};

export default Portfolio;
