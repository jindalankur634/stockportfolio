import React, { useEffect, useState } from "react";
import Navbar from "./Navbar";
import stock from "../data/stock.json";
import {
  Card,
  CardFooter,
  CardSubtitle,
  CardTitle,
  Modal,
  ModalBody,
  Button,
  ModalFooter,
} from "reactstrap";
import StockFullDetail from "./StockFullDetail";
import { useNavigate, useLocation } from "react-router-dom";

const Home = () => {
  const location = useLocation();
  const quantity = location.pathname.split("/")[2];
  const buy_price = location.pathname.split("/")[3];
  const id = location.pathname.split("/")[1];
  const token = localStorage.getItem("token");

  const StockData = {
    QUANTITY: "",
    COMPANY: "",
    BUY_PRICE: "",
    TODAY_PRICE: "",
    SYMBOL: "",
  };
  const [allStocks, setAllStocks] = useState([StockData]);
  const navigate = useNavigate();

  // useEffect(() => {
  //   console.log(token,'yes')
  //   if(!token){
  //     navigate('/login')
  //   }
  // },[])

  useEffect(() => {
    settingStockData();
  }, [quantity, buy_price]);

  function settingStockData() {
    if (id) {
      const filterData = stock.filter((stock) => stock.id == id);
      StockData.BUY_PRICE = buy_price;
      StockData.QUANTITY = quantity;
      StockData.COMPANY = filterData[0].company;
      StockData.SYMBOL = filterData[0].symbol;
      StockData.TODAY_PRICE = filterData[0].today_price;

      setAllStocks({ ...allStocks, ...StockData });
    }
  }

  return (
    <div className="bg-dark overflow-auto">
      <Navbar />
      <div className=" m-5">
        {stock.map((e, idx) => {
          return (
            <Card
              body
              inverse
              style={{
                backgroundColor: "#333",
                borderColor: "#",
              }}
              className="m-1"
            >
              <CardTitle className="display-6 d-flex flex-wrap">
                {e.company}
                <div className="today-price">{e.today_price}</div>
              </CardTitle>
              <CardSubtitle className="d-flex">
                {e.symbol}
                <div className="diff-price">
                  {e.today_price - e.yesterday_price > 0 ? (
                    <span>
                      +{(e.today_price - e.yesterday_price).toFixed(2)} ( +
                      {(
                        ((e.today_price - e.yesterday_price) /
                          e.yesterday_price) *
                        100
                      ).toFixed(2)}
                      )
                    </span>
                  ) : (
                    <span>
                      {(e.today_price - e.yesterday_price).toFixed(2)} ( -
                      {(
                        (100 / e.today_price) * e.today_price -
                        e.yesterday_price
                      ).toFixed(2)}
                      )
                    </span>
                  )}
                </div>
              </CardSubtitle>
              <button
                className="btn btn-dark mt-4"
                onClick={() => {
                  navigate(`/stock/${e.symbol}`);
                  // <StockFullDetail />;
                }}
              >
                Show Details
              </button>
            </Card>
          );
        })}
      </div>
    </div>
  );
};

export default Home;
