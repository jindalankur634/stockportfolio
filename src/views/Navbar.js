import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
const Navigation = () => {
  toast.configure();
  let navigate = useNavigate();
  const [toggle, setToggle] = useState(false);

  function logout() {
    navigate("/login");
    toast.success("Logout Successfully!");
  }

  function toggleNavbar() {
    setToggle(!toggle);
  }
  return (

    <div>
      <Navbar color="faded" dark>
        <NavbarBrand href="/" className="mr-auto">
          Stock Market
        </NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} className="mr-2" />
        <Collapse isOpen={toggle} navbar>
          <Nav navbar>
            <NavItem>
              <NavLink href="/">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/portfolio">Portfolio</NavLink>
            </NavItem>
            <NavItem>
              <NavLink  onClick={logout}>
                Logout
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Navigation;
