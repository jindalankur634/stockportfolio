import React, { useState } from "react";
import { CanvasJSChart } from "canvasjs-react-charts";
import { Button, Input, Modal, ModalHeader, Col, Row, Label } from "reactstrap";
import stockPrices from "../data/chart.json";
import Navbar from "./Navbar";
import { toast } from "react-toastify";
const GraphView = ({ setGraphView, graphView, compName }) => {
  toast.configure();
  const [fromDate, setFromDate] = useState("");
  const [toDate, setToDate] = useState("");
  const [stockPrice, setStockPrice] = useState(stockPrices);
  const options = {
    animationEnabled: true,
    exportEnabled: true,
    theme: "light2", // "light1", "dark1", "dark2"
    title: {
      text: `${compName} Stock Chart`,
    },
    subtitles: [
      {
        text: "Stock Price (in USD)",
      },
    ],
    axisY: {
      title: "Price",
      suffix: "$",
    },
    axisX: {
      title: "Date",
      prefix: "",
      interval: 1,
    },
    data: [
      {
        type: "spline",
        // toolTipContent: "Week {x}: {y}%",
        dataPoints: stockPrice.map((stockPrices) => ({
          x: stockPrices.x,
          y: stockPrices.y,
        })),
      },
    ],
  };

  function handleChange(e, text) {
    let daate = e.target.value;
    let day = daate.split("-")[2];
    if (text == "from") {
      setFromDate(day);
    } else if (text == "to") {
      setToDate(day);
    }
  }

  function handleSearch() {
    console.log(fromDate);
    if (fromDate != "" && toDate != "") {
      let filterData = stockPrices.filter(
        (e) => e.x >= fromDate && e.x <= toDate
      );
      setStockPrice(filterData);
    } else {
      toast.error("Please Enter Both Dates");
    }
  }

  return (
    <div>
      <Modal isOpen={true} size="xl">
        <Row className="col-12">
          <Col
            className="col-1 p-1"
            onClick={() => setGraphView(!graphView)}
            className=" closeButton d-flex justify-content-end"
          >
            x
          </Col>
        </Row>
        <Row className="m-2 col-12">
          <Col className="col-5">
            <Row>
              <Col className="h4 d-flex justify-content-end">From</Col>
              <Col className="">
                <Input type="date" onChange={(e) => handleChange(e, "from")} />
              </Col>
            </Row>
          </Col>
          <Col className="col-4">
            <Row>
              <Col className="h4">To</Col>
              <Col className="">
                <Input type="date" onChange={(e) => handleChange(e, "to")} />
              </Col>
            </Row>
          </Col>
          <Col className="col-3">
            <Button className="btn btn-success" onClick={() => handleSearch()}>
              Search
            </Button>
          </Col>
        </Row>
        <CanvasJSChart options={options} />
      </Modal>
    </div>
  );
};

export default GraphView;
